<?php

  session_start();
  require_once 'functions/sessions.php';
  require_once 'functions/forms.php';

  if(formWasSubmitted()) {
    if(formWasValid()) {
      loginUser($_POST['username']);
    }
  }
 ?>

<form method="post">
  <label>Enter Your Name</label>
  <input type="text" name="username" />
  <button>Submit</button>
</form>
