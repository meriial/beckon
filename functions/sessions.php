<?php

function loginUser($username)
{
  $_SESSION['username'] = $username;
  redirectToBeckon();
}

function userIsLoggedIn()
{
  $return = !empty($_SESSION['username']);
  return $return;
}

function loggedInUserName()
{
  return $_SESSION['username'];
}

function redirectToLogin()
{
  header('Location: login.php');
}


function redirectToBeckon()
{
  header('Location: index.php');
}
