<?php

function formWasSubmitted()
{
  return !empty($_POST);
}

function formWasValid()
{
  return !empty($_POST['username']);
}
