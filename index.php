<?php

  session_start();

  require_once 'functions/sessions.php';

  if(!userIsLoggedIn()) {
    redirectToLogin();
  }

 ?>
<html>
  <head>
    <title>Meriial Help!</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>
  </head>
  <body>
    <h1>Beckon</h1>
    <div>Hello, <?php print loggedInUserName() ?></div>
    <a href="logout.php">logout</a>
    <form method="post" action="functions/submitBeckon.php" id="beckon-form">
      <input name="name" id="name"/>
      <input type="submit" id="submit" value="Submit"/>
    </form>
    <div id="beckon">
      <!-- content will be updated constantly by AJAX -->
    </div>
  </body>
</html>
