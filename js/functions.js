/*
 * @author Victor Fan
 * 
 */

var DEBUG = false;

function getFormattedData() {
  $.ajax({
    url: 'functions/getFormattedData.php',
    success: function (data) {
      if (DEBUG) {
        console.log(data);
      }
      $("#beckon").html(data);
    }
  });
}

function updateBeckon() {
  getFormattedData();
}

function clearForm() {
  $("#name").val("");
}

function ajaxSubmit() {
  var name = $("#name").val();

  $.ajax({
    type: "POST",
    url: "functions/submitBeckon.php",
    data: {
      name: name
    },
    success: function () {
      clearForm();
      $("#name").focus();
    }
  });
}

$(function () {
  $("#name").focus();
// Periodically update beckons
  setInterval(function () {
    updateBeckon();
  }, 500);

  // Handles both mouse click and keyboard enter
  $("#beckon-form").submit(function (event) {
    ajaxSubmit();
    event.preventDefault();
  });
});