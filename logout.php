<?php

  require_once 'functions/sessions.php';
  
  session_start();
  session_unset();
  session_destroy();

  redirectToLogin();
